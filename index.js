// console.log("Hello World!");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	//first function here:
function userDetails() {
		let fullName = prompt("Enter your Full Name: ");
		let age = prompt("Enter your Age: ");
		let location = prompt("Enter your Location: ");

		console.log("Hello, " + fullName + "! \nYou are " + age + " years old.\nYou live in " + location);
		
	};
	
userDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

function myFavBands() {
	let favBand1 = "1. Westlife";
	let favBand2 = "2. Backstreet Boys";
	let favBand3 = "3. NSYNC";
	let favBand4 = "4. Starset";
	let favBand5 = "5. Casting Crowns";

	console.log(favBand1);
	console.log(favBand2);
	console.log(favBand3);
	console.log(favBand4);
	console.log(favBand5);
}

myFavBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

function myFavMovies() {
	let title1 = "1. Black Adam";
	let rating1 = 40;
	let title2 = "2. Top Gun: Maverick";
	let rating2 = 96;
	let title3 = "3. Morbius";
	let rating3 = 15;
	let title4 = "4. Inception";
	let rating4 = 87;
	let title5 = "5. Shutter Island";
	let rating5 = 68;

	console.log(title1 + "\nRotten Tomatoes Rating: " + rating1 + "%");
	console.log(title2 + "\nRotten Tomatoes Rating: " + rating2 + "%");
	console.log(title3 + "\nRotten Tomatoes Rating: " + rating3 + "%");
	console.log(title4 + "\nRotten Tomatoes Rating: " + rating4 + "%");
	console.log(title5 + "\nRotten Tomatoes Rating: " + rating5 + "%");

};

myFavMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/
// printUsers();
let printFriends = function printUsers() {

	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);